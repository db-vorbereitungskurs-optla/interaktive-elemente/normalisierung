const $ = require('jquery');

$(document).ready(() => {
    /**
     * false: Eine Auswahl dieser Spalte wäre falsch
     * true: Eine Auswahl dieser Spalte wäre richtig
     */
    const nf_solution = {

        unf: {
            true: ['head_name', 'head_gta_nr', 'head_description', 'head_h_completed', 'head_class_lead'],
            false: ['head_s_nr', 'head_class'],
        },

        nf1: {
            true: ['head_forename', 'head_lastname', 'head_class', 'head_description', 'head_class_lead', 'head_class_lead_gender'],
            false: ['head_s_nr', 'head_gta_nr', 'head_h_completed'],
        },

        nf2: {
            true: ['stud_head_class', 'stud_head_class_lead', 'stud_head_class_lead_gender'],
            false: ['stud_head_s_nr', 'stud_head_forename', 'stud_head_lastname', 'gtaAng_head_gta_nr',
                'gtaAng_head_description', 'gta_head_s_nr', 'gta_head_gta_nr', 'gta_head_h_completed'
            ],
        },
    };

    const nf_sol_feedback = {

        correct: {
            msg: 'Gl&uuml;ckwunsch, Du hast alles richtig gew&auml;hlt !',
            color: 'rgb(0, 255, 0)',
        },

        part_false: {
            msg: 'Du hast noch nicht alles richtig. Deine richtig gefundenen Spalten sind in der Auswahl erhalten geblieben.',
            color: 'rgb(255, 165, 0)',
        },

        false: {
            msg: 'Du hast leider nichts richtig.',
            color: 'rgb(255, 0, 0)',
        },

        selected_all: {
            msg: 'Du darfst nicht alle Spalten markieren. Versuche es noch einmal.',
            color: 'rgb(255, 165, 0)',
        },
    };

    const correct_feedback = {
        'unf': '<br>Für die 1.Normalform gilt:<br>- alle Attribute enthalten atomare Werte<br>- jede Zeile ist durch ein Schlüsselattribut oder eine Attributkombination eindeutig identifiziert',
        'nf1': '<br>Für die 2.Normalform gilt:<br>- die Relation ist in der 1. Normalform<br>- jedes Nichtschlüsselattribut ist von jedem Schlüsselkandidaten voll funktional abhängig',
        'nf2': '<br>Für die 3.Normalform gilt:<br>- die Relation ist in der 2. Normalform<br>- kein Nichtschlüsselattribut ist von einem Schlüsselkandidaten transitiv abhängig'
    }

    const nf_info_text = {

        nf1: 'Die Tabelleneinträge wurden nun auf die Erste Normalform normalisiert. Allerdings reicht nun die <b>S_Nr</b> zur eindeutigen Identifikation nicht mehr aus.',
        nf2: 'Die Tabelle wurde in ihre Relationen aufgegliedert und f&uuml;r jede Relation eine eigene Tabelle angelegt',
        nf3: '',
    };

    /*
        DATA
    */

    /*
        NF 1
    */
    // Head Name, id
    const table_header_nf1 = [
        ['S_Nr', 'head_s_nr'],
        ['Vorname', 'head_forename'],
        ['Name', 'head_lastname'],
        ['Klasse', 'head_class'],
        ['KL_Name', 'head_class_lead'],
        ['KL_Anrede', 'head_class_lead_gender'],
        ['GTA_Nr', 'head_gta_nr'],
        ['Beschreibung', 'head_description'],
        ['h_absolviert', 'head_h_completed'],
    ];

    const table_rows_nf1 = [
        ['1', 'Luca', 'Kluge', '10b', 'Herzog', 'Herr', '1', 'Schach', '2'],
        ['1', 'Luca', 'Kluge', '10b', 'Herzog', 'Herr', '5', 'Fotografie', '10'],
        ['2', 'Katrin', 'Ackermann', '9a', 'Baer', 'Frau', '2', 'Tastaturschreiben', '8'],
        ['3', 'Uwe', 'Boehm', '8c', 'Kastner', 'Herr', '1', 'Schach', '1'],
        ['3', 'Uwe', 'Boehm', '8c', 'Kastner', 'Herr', '3', 'Jugend forscht', '12'],
        ['4', 'Philipp', 'Freud', '10a', 'Baumgartner', 'Frau', '4', 'Schuelerfirma', '2'],
        ['4', 'Philipp', 'Freud', '10a', 'Baumgartner', 'Frau', '6', 'Handwerk', '15'],
        ['5', 'Carolin', 'Kruger', '10b', 'Herzog', 'Herr', '5', 'Fotografie', '30'],
        ['6', 'David', 'Maier', '9a', 'Baer', 'Frau', '6', 'Handwerk', '8'],
        ['6', 'David', 'Maier', '9a', 'Baer', 'Frau', '8', 'Erste Hilfe', '6'],
        ['7', 'Lisa', 'Schwarz', '8c', 'Kastner', 'Herr', '5', 'Fotografie', '5'],
        ['7', 'Lisa', 'Schwarz', '8c', 'Kastner', 'Herr', '7', 'Programmierung', '25'],
        ['8', 'Sandra', 'Ziegler', '10b', 'Herzog', 'Herr', '2', 'Tastaturschreiben', '7'],
        ['8', 'Sandra', 'Ziegler', '10b', 'Herzog', 'Herr', '8', 'Erste Hilfe', '2'],
    ];

    /*
        NF 2
    */
    const table_header_nf2 = {
        rel_stud: [
            ['S_Nr', 'stud_head_s_nr'],
            ['Vorname', 'stud_head_forename'],
            ['Name', 'stud_head_lastname'],
            ['Klasse', 'stud_head_class'],
            ['KL_Name', 'stud_head_class_lead'],
            ['KL_Anrede', 'stud_head_class_lead_gender']
        ],

        rel_gta_ang: [
            ['GTA_Nr', 'gtaAng_head_gta_nr'],
            ['Beschreibung', 'gtaAng_head_description']
        ],

        rel_gta: [
            ['S_Nr', 'gta_head_s_nr'],
            ['GTA_Nr', 'gta_head_gta_nr'],
            ['h_absolviert', 'gta_head_h_completed']
        ],
    };

    const table_rows_nf2 = {
        rel_stud: [
            ['1', 'Luca', 'Kluge', '10b', 'Herzog', 'Herr'],
            ['2', 'Katrin', 'Ackermann', '9a', 'Baer', 'Frau'],
            ['3', 'Uwe', 'Boehm', '8c', 'Kastner', 'Herr'],
            ['4', 'Philipp', 'Freud', '10a', 'Baumgartner', 'Frau'],
            ['5', 'Carolin', 'Kruger', '10b', 'Herzog', 'Herr'],
            ['6', 'David', 'Maier', '9a', 'Baer', 'Frau'],
            ['7', 'Lisa', 'Schwarz', '8c', 'Kastner', 'Herr'],
            ['8', 'Sandra', 'Ziegler', '10b', 'Herzog', 'Herr']
        ],

        rel_gta_ang: [
            ['1', 'Schach'],
            ['2', 'Tastaturschreiben'],
            ['3', 'Jugend forscht'],
            ['4', 'Schuelerfirma'],
            ['5', 'Fotografie'],
            ['6', 'Handwerk'],
            ['7', 'Programmierung'],
            ['8', 'Erste Hilfe']
        ],

        rel_gta: [
            [1, 1, 2],
            [1, 5, 10],
            [2, 2, 8],
            [3, 1, 1],
            [3, 3, 12],
            [4, 4, 2],
            [4, 6, 15],
            [5, 5, 30],
            [6, 6, 8],
            [6, 8, 6],
            [7, 5, 5],
            [7, 7, 25],
            [8, 2, 7],
            [8, 8, 2]
        ],
    };

    /*
        NF 3

    */

    const table_header_nf3 = {
        rel_stud: [
            ['S_Nr'],
            ['Vorname'],
            ['Name'],
            ['Klasse']
        ],
        rel_gta_ang: [
            ['GTA_Nr'],
            ['Beschreibung']
        ],
        rel_gta: [
            ['S_Nr'],
            ['GTA_Nr'],
            ['h_absolviert']
        ],
        rel_class: [
            ['Klasse'],
            ['KL_Name'],
            ['KL_Anrede']
        ],
    };

    const table_rows_nf3 = {
        rel_stud: [
            ['1', 'Luca', 'Kluge', '10b'],
            ['2', 'Katrin', 'Ackermann', '9a'],
            ['3', 'Uwe', 'Boehm', '8c'],
            ['4', 'Philipp', 'Freud', '10a'],
            ['5', 'Carolin', 'Kruger', '10b'],
            ['6', 'David', 'Maier', '9a'],
            ['7', 'Lisa', 'Schwarz', '8c'],
            ['8', 'Sandra', 'Ziegler', '10b']
        ],

        rel_gta_ang: [
            ['1', 'Schach'],
            ['2', 'Tastaturschreiben'],
            ['3', 'Jugend forscht'],
            ['4', 'Schuelerfirma'],
            ['5', 'Fotografie'],
            ['6', 'Handwerk'],
            ['7', 'Programmierung'],
            ['8', 'Erste Hilfe']
        ],

        rel_gta: [
            [1, 1, 2],
            [1, 5, 10],
            [2, 2, 8],
            [3, 1, 1],
            [3, 3, 12],
            [4, 4, 2],
            [4, 6, 15],
            [5, 5, 30],
            [6, 6, 8],
            [6, 8, 6],
            [7, 5, 5],
            [7, 7, 25],
            [8, 2, 7],
            [8, 8, 2]
        ],

        rel_class: [
            ['9a', 'Baer', 'Frau'],
            ['8c', 'Kastner', 'Herr'],
            ['10b', 'Herzog', 'Herr'],
            ['10a', 'Baumgartner', 'Frau']
        ],
    };

    // enthält ausgewählte Header
    let selected_header = [];

    // zu Beginn wird unnormalisierte Normalform gezeigt
    let actual_nf = 'unf';

    const nfBtnCheck = $('#nf_btn_check');
    const nf_btn_next = $('#nf_btn_next');

    nfBtnCheck.click(check_sel_columns);
    nf_btn_next.click(show_next_task);

    // Weise den Spalten-Headern die onClick-Methode zu

    // füge ausgewählte Head hinzu
    // choice = GesamtElement
    const th_switch = function() {
        if (!contains(this.id)) {
            add_column(this);
        } else {
            remove_column(this);
        }
    };

    $('#head_s_nr').click(th_switch);
    $('#head_name').click(th_switch);
    $('#head_class').click(th_switch);
    $('#head_class_lead').click(th_switch);
    $('#head_gta_nr').click(th_switch);
    $('#head_description').click(th_switch);
    $('#head_h_completed').click(th_switch);

    // ///////////////////////
    // // HELPING FUNCTIONS //
    // ///////////////////////

    function remove_feedback() {
        if (feedback_div.hasChildNodes()) {
            feedback_div.removeChild(feedback_div.lastChild);
        }
    }

    function contains(column_id) {
        return (selected_header.indexOf(column_id) > -1);
    }

    // Entfernt ausgewählten Header
    function remove_column(choice) {
        const pos = selected_header.indexOf(choice.id);
        if (pos > -1) {
            selected_header.splice(pos, 1);
        }
        choice.style.backgroundColor = '';
    }

    // Fügt ausgewählten Header hinzu
    function add_column(choice) {
        selected_header.push(choice.id);
        choice.setAttribute('style', 'background: #4CAf50');
    }

    function set_info(nf) {
        const nf_info_p = document.getElementById('nf-info');
        nf_info_p.innerHTML = nf_info_text[nf];
    }

    /*  gibt bestimmtes lösungs-array zurück:
        - nach Normalform (str_nf)
        - nach richtig/falsch (str_bool)
    */
    function get_specific_array(str_nf, str_bool) {
        return nf_solution[str_nf][str_bool];
    }

    /*  prüft, ob selected columns/header der Lösung entsprechen
     */
    function check_sel_and_sol(solution_array) {
        solution_array.sort();
        selected_header.sort();
        if (solution_array.length === selected_header.length) {
            for (let i = 0; i <= solution_array.length; i += 1) {
                if (solution_array[i] !== selected_header[i]) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    // prüft, ob header-Array Arrays aus 2 Elementen [Header-Name, id] besteht
    function header_has_id(headers) {
        if (headers[0].length > 1) {
            return true;
        }
        return false;
    }

    function create_table_headline(table_title) {
        const div_headline = document.createElement('div');
        div_headline.className = 'sub-head-text'

        //div_headline.style.borderBottom = '0.1em solid #efefef';
        div_headline.style.margin = '2em 0 1em 0';

        div_headline.innerHTML = table_title;

        return div_headline;
    }

    function create_under_row() {
        const div_row = document.createElement('div');
        div_row.className = 'row';

        const div_headlne = create_table_headline('Relation GTA');
        const tab_rel = create_table(table_header_nf2['rel_gta'], table_rows_nf2['rel_gta']);

        const div_rel = document.createElement('div');
        div_rel.style.marginTop = '1em';

        div_rel.appendChild(div_headlne);
        div_rel.appendChild(tab_rel);

        div_row.appendChild(div_rel);

        return div_row;
    }

    // #######################################
    // ## FUNCTIONS USING HELPING FUNCTIONS ##
    // #######################################

    // Zeigt die nächste Aufgabe an und setzt Variablen zurück
    function show_next_task() {
        nf_btn_next.attr('hidden', true);
        remove_feedback();
        selected_header = [];

        // setzt globale Variable actual_nf neu
        load_new_table();
        set_info(actual_nf);
    }

    function give_feedback(str_feed) {
        const feedback_div = document.getElementById('feedback_div');

        // jederzeit darf nur max 1 Feedback gerendert werden
        remove_feedback();

        // Erstelle neues feedback
        const feedback_element = document.createElement('div');
        feedback_element.className = 'nf-feedback';
        feedback_element.style.borderColor = nf_sol_feedback[str_feed].color;
        feedback_element.innerHTML = nf_sol_feedback[str_feed].msg;
        if(str_feed === "correct"){
            feedback_element.innerHTML += correct_feedback[actual_nf];
        }
        feedback_div.appendChild(feedback_element);
    }

    // Prüft ausgewählte Spalten auf Richtigkeit
    function check_sel_columns() {
        const solution_array = get_specific_array(actual_nf, 'true');

        // stimmen Lösungsarray und columns array überein?
        if (check_sel_and_sol(solution_array)) {
            give_feedback('correct');
            nf_btn_next.attr('hidden', false);
            // nf_btn_next.hidden = false;
        } else {
            wrong_array = get_specific_array(actual_nf, 'false');
            // falls alle Spalten ausgewählt wurden, setze Auswahl zurück
            if (selected_header.length == solution_array.length + wrong_array.length) {
                const selected_headers = selected_header.splice(0);
                for (let i = 0; i < selected_headers.length; i++) {
                    remove_column(document.getElementById(selected_headers[i]));
                }
                give_feedback('selected_all');
            } else {
                // falls Lösung nicht korrekt, nur Spalten markiert lassen, die Lösung entsprechen
                for (let i = 0; i <= wrong_array.length; i += 1) {
                    const wrong_col = wrong_array[i];
                    if (contains(wrong_col)) {
                        remove_column(document.getElementById(wrong_col));
                    }
                }
                if (selected_header.length !== 0) {
                    give_feedback('part_false');
                } else {
                    give_feedback('false');
                }
            }
        }
    }

    // ###################################
    // ## FUNCTIONS FOR BUILDING TABLES ##
    // ###################################

    /**
     *
     * @param {string} nf
     * @param {arrayOfStrings} rel_names
     * @param {arrayOfStrings} rel_titles
     * @param {arrayOfStrings} styles - enthält margin-Werte für die Tabellen
     */
    function create_row_multiple_tables(nf, rel_names, rel_titles, styles) {
        let table_header;

        if (nf === 'nf2') {
            table_header = table_header_nf2;
            table_rows = table_rows_nf2;
        } else {
            table_header = table_header_nf3;
            table_rows = table_rows_nf3;
        }

        const div_row = document.createElement('div');
        div_row.className = 'row';

        for (let i = 0; i < rel_names.length; i += 1) {
            const div_headline = create_table_headline(rel_titles[i]);
            const tab_rel = create_table(table_header[rel_names[i]], table_rows[rel_names[i]]);

            const div_rel = document.createElement('div');
            div_rel.style.margin = styles[i];

            div_rel.appendChild(div_headline);
            div_rel.appendChild(tab_rel);

            div_row.appendChild(div_rel);
        }

        return div_row;
    }

    function create_tableArea(nf) {
        const div_grid = document.createElement('div');
        div_grid.className = 'container';

        // Die Realtionen der oberen Reihe
        let rel_names = ['rel_stud', 'rel_gta_ang'];
        let rel_titles = ['Relation Sch&uuml;ler', 'Relation_GTA_Angebot'];
        let styles = ['0 1em 0 0', '0'];

        div_row_1 = create_row_multiple_tables(nf, rel_names, rel_titles, styles);
        div_grid.appendChild(div_row_1);

        if (nf === 'nf2') {
            div_row_2 = create_under_row();
        } else {
            rel_names = ['rel_gta', 'rel_class'];
            rel_titles = ['Relation GTA', 'Relation Klasse'];
            styles = ['1em 1em 0 0', '1em 0 0 0'];
            div_row_2 = create_row_multiple_tables(nf, rel_names, rel_titles, styles);
        }
        div_grid.appendChild(div_row_2);

        return div_grid;
    }

    function load_new_table() {
        const tab_area = document.getElementById('tablearea');

        tab_area.innerHTML = '';
        const title = document.getElementById('interactive_title');

        if (actual_nf === 'nf2') {
            actual_nf = 'nf3';
            tab_area.appendChild(create_tableArea(actual_nf));
            title.innerHTML = 'Dritte Normalform (3NF)';
            nfBtnCheck.attr('hidden', true);
            const task_div = document.getElementById('nf-task');
            task_div.hidden = true;
        }

        if (actual_nf === 'nf1') {
            actual_nf = 'nf2';
            tab_area.appendChild(create_tableArea(actual_nf));
            title.innerHTML = 'Zweite Normalform (2NF)';
            nf_btn_next.html('L&ouml;sung');
        }
        if (actual_nf === 'unf') {
            actual_nf = 'nf1';
            tab_area.appendChild(create_table(table_header_nf1, table_rows_nf1));
            title.innerHTML = 'Erste Normalform (1NF)';
        }
    }

    /*
        Die folgenden Tabellen werden zur Laufzeit generiert
    */

    function create_table(table_header, table_rows) {
        const table = document.createElement('table');

        // Erstelle die Table-head
        thead = document.createElement('thead');
        const tr_head = document.createElement('tr');
        for (let i = 0; i < table_header.length; i += 1) {
            let th;
            if (header_has_id(table_header)) {
                th = document.createElement('th');
                th.id = table_header[i][1];
                th.onclick = th_switch;
            } else {
                th = document.createElement('td');
                th.style.fontWeight = 'bold';
            }
            const text = document.createTextNode(table_header[i][0]);

            th.appendChild(text);
            tr_head.appendChild(th);
        }
        thead.appendChild(tr_head);
        table.appendChild(thead);

        // Erstelle die Table-rows
        tbody = document.createElement('tbody');
        for (let i = 0; i < table_rows.length; i += 1) {
            const tr = document.createElement('tr');
            row = table_rows[i];

            for (let j = 0; j < row.length; j += 1) {
                const td = document.createElement('td');
                const text = document.createTextNode(row[j]);

                td.appendChild(text);
                tr.appendChild(td);
            }
            tbody.appendChild(tr);
        }
        table.appendChild(tbody);

        // style
        table.classList.add('table', 'table-sm', 'table-bordered', 'table-striped', 'nf-table');

        return table;
    }
});